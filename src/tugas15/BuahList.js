import React, {useContext} from "react"
import { ListContext } from "./ListContext"
import axios from "axios"

const BuahList = () =>{
  const [
  dataHargaBuah, setDataHargaBuah,
  inputName, setInputName,
  inputPrice, setInputPrice,
  inputWeight, setInputWeight,
  selectedId, setSelectedId,
  statusForm, setStatusForm] = useContext(ListContext)

  const handleDelete = (event) => {
    let idBuah = parseInt(event.target.value)

    let newDataHargaBuah = dataHargaBuah.filter(item => item.id !== idBuah)
    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
      .then(res=>{
        console.log(res);
      })

      setDataHargaBuah([...newDataHargaBuah])
  }
  
  const handleEdit = (event) =>{
    let idBuah = parseInt(event.target.value)
    let buah = dataHargaBuah.find(x=> x.id === idBuah)

    setInputName(buah.name)
    setInputPrice(buah.price)
    setInputWeight(buah.weight)
    setSelectedId(idBuah)
    setStatusForm("edit")
  }

  return(
    <>
      <h1>Tabel Harga Buah</h1>
      <table>
          <thead>
              <tr>
                  <th>Nama</th>
                  <th>Harga</th>
                  <th>Berat</th>
                  <th>Aksi</th>
              </tr>
          </thead>
          <tbody>
              {
                dataHargaBuah !== null && dataHargaBuah.map((data, index)=>{
                  return(                    
                    <tr key={index}>
                        <td>{data.name}</td>
                        <td>{data.price}</td>
                        <td>{data.weight/1000} kg</td>
                        <td>
                            <button onClick={handleEdit} value={data.id}>Edit</button>
                            &nbsp;
                            <button onClick={handleDelete} value={data.id}>Delete</button>
                        </td>
                    </tr>
                  )
                })
              }
          </tbody>
      </table>
    </>
  )

}

export default BuahList