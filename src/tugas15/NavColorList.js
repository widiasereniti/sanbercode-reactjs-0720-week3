import React, {useContext} from "react"
import {Link} from "react-router-dom";
import {NavColorContext} from "./NavColorContext"

const NavColorList = () =>{
  const [color] = useContext(NavColorContext)

  return(
    // <ul>
    //   {movie.map(el=>{
    //     return <li>name: {el.name} {el.lengthOfTime} minutes</li>
    //   })}
    // </ul>
    <nav>
        <ul>
            <li>
                <Link to="/tugas11">Tugas 11</Link>
            </li >
            <li>
                <Link to="/tugas12">Tugas 12 (Timer)</Link>
            </li>
            <li>
                <Link to="/tugas13">Tugas 13 (Class List)</Link>
            </li>
            <li>
                <Link to="/tugas14">Tugas 14 (Hooks List)</Link>
            </li>
            <li>
                <Link to="/tugas15">Tugas 15 (Context List)</Link>
            </li>
        </ul>
    </nav>
  )

}

export default NavColorList