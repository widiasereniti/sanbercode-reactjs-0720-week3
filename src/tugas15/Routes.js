import React from "react";
import { Switch, Route, Link } from "react-router-dom";

import Tugas11 from '../tugas11/index'
import Tugas12 from '../tugas12/index'
import Tugas13 from '../tugas13/index'
import Tugas14 from '../tugas14/index'
import Tugas15 from '../tugas15/Buah'

const Routes = () => {
    return (
        <>
          <nav>
              <ul>
                  <li>
                    <Link to="/tugas11">Tugas 11</Link>
                  </li >
                  <li>
                    <Link to="/tugas12">Tugas 12 (Timer)</Link>
                  </li>
                  <li>
                    <Link to="/tugas13">Tugas 13 (Class List)</Link>
                  </li>
                  <li>
                    <Link to="/tugas14">Tugas 14 (Hooks List)</Link>
                  </li>
                  <li>
                    <Link to="/tugas15">Tugas 15 (Context List)</Link>
                  </li>
              </ul>
          </nav>
          <Switch>
              <Route path="/tugas11">
                <Tugas11 />
              </Route>
              <Route path="/tugas12">
                <Tugas12 />
              </Route>
              <Route path="/tugas13">
                <Tugas13 />
              </Route>
              <Route path="/tugas14">
                <Tugas14 />
              </Route>
              <Route exact path="/tugas15">
                <Tugas15 />
              </Route>
          </Switch>
        </>
    );
  };
  
export default Routes;