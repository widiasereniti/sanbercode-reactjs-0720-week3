import React from "react"
import {ColorNavProvider} from "./ColorNavContext"
import NavColorList from "./NavColorList"

const NavColor = () =>{
  return(
    <ColorNavProvider>
      <NavColorList/>
    </ColorNavProvider>
  )
}

export default NavColor
