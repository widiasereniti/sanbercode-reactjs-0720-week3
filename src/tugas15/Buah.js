import React from "react"
import {ListProvider} from "./ListContext"
import BuahList from "./BuahList"
import BuahForm from "./BuahForm"

const Buah = () =>{
  return(
    <ListProvider>
      <BuahList />
      <BuahForm />
    </ListProvider>
  )
}

export default Buah