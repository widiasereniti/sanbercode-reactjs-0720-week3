import React, {useContext, useState} from "react"
import {ListContext} from "./ListContext"
import axios from "axios";

const BuahForm = () =>{

  const [
    dataHargaBuah, setDataHargaBuah,
    inputName, setInputName,
    inputPrice, setInputPrice,
    inputWeight, setInputWeight,
    selectedId, setSelectedId,
    statusForm, setStatusForm
  ] = useContext(ListContext)

  const handleChange = (event) =>{

    const { name, value } = event.target
    switch(name){
        case "inputName":
            setInputName(value);
            break;
        case "inputPrice":
            setInputPrice(value);
            break;
        case "inputWeight":
            setInputWeight(value);
            break;
        default:
            break;
    }
  }

  const handleSubmit = (event) =>{

    event.preventDefault()
    let name = inputName
    let price = inputPrice
    let weight = inputWeight

    if (name.replace(/\s/g,'') !== "" && price.toString().replace(/\s/g,'') !== "" && weight.toString().replace(/\s/g,'') !== ""){
      if (statusForm === "create"){
        axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name, price, weight})
        .then(res => {
            setDataHargaBuah([...dataHargaBuah, {
              id: res.data.id,
              name: name,
              price: price,
              weight: weight
            }])
        })

      } else if(statusForm === "edit"){
        axios.put(`http://backendexample.sanbercloud.com/api/fruits/${selectedId}`, {name, price, weight})
        .then(res => {
            let dataBuah = dataHargaBuah.find(el=> el.id === selectedId)
            dataBuah.name = name
            dataBuah.price = price
            dataBuah.weight = weight
            setDataHargaBuah([...dataHargaBuah])
        })
      }

      setStatusForm("create")
      setSelectedId(0)
      setInputName("")
      setInputPrice("")
      setInputWeight("")
    }
  }

  return(
    <>
      <h1>Form Buah</h1>
      <form onSubmit={handleSubmit}>
          <label>Nama Buah:</label>          
          <input 
              type="text"
              name="inputName"
              value={inputName}
              onChange={handleChange}
              placeholder="Nama Buah"
          /><br/>
          <label>Harga Buah:</label>          
          <input 
              type="text" 
              name="inputPrice"
              value={inputPrice}
              onChange={handleChange}
              placeholder="Harga Buah (Ribuan)"
          /><br/>
          <label>Berat Buah: </label>          
          <input 
              type="text"
              name="inputWeight"
              value={inputWeight}
              onChange={handleChange}
              placeholder="Berat Buah (gr)"
          /><br/>
          <button>submit</button>
      </form>
    </>
  )

}

export default BuahForm
