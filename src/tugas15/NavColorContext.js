import React, { useState, createContext } from "react";

export const NavColorContext = createContext();
export const NavColorProvider = props => {
  const [color, setColor] =  useState([
    "blue",
    "black",
    "red",
    "grey",
    "yellow"
  ])

  return (
    <NavColorContext.Provider value={[color, setColor]}>
      {props.children}
    </NavColorContext.Provider>
  );
}