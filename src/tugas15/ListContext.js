import React, { useState, createContext } from "react";
import axios from "axios"

export const ListContext = createContext();

export const ListProvider = props => {
  const [dataHargaBuah, setDataHargaBuah] = useState(null);
  const [inputName, setInputName] = useState("")
  const [inputPrice, setInputPrice] = useState("")
  const [inputWeight, setInputWeight] = useState("")
  const [selectedId, setSelectedId]  =  useState(0)
  const [statusForm, setStatusForm]  =  useState("create")

  if (dataHargaBuah === null){
    axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
      .then(res => {
        setDataHargaBuah(res.data.map(item=>{ return {
          id: item.id,
          name: item.name,
          price: item.price,
          weight: item.weight}} ))
      })
  }

  return (
    <ListContext.Provider value = {[
        dataHargaBuah, setDataHargaBuah,
        inputName, setInputName,
        inputPrice, setInputPrice,
        inputWeight, setInputWeight,
        selectedId, setSelectedId,
        statusForm, setStatusForm
    ]}>
        {props.children}
    </ListContext.Provider>
  );
};
