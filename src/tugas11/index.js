import React from 'react';
import './index.css'

// Tugas 11 - Reactjs Components & Props

class FruitsName extends React.Component {
  render() {
    return <td className="fruitsname">{this.props.name}</td>;
  }
}

class FruitsPrice extends React.Component {
  render() {
    return <td>{this.props.price}</td>;
  }
}

class FruitsWeight extends React.Component {
  render() {
    return <td>{(this.props.weight / 1000)} kg </td>;
  }
}

let dataHargaBuah = [
  {nama: "Semangka", harga: 10000, berat: 1000},
  {nama: "Anggur", harga: 40000, berat: 500},
  {nama: "Strawberry", harga: 30000, berat: 400},
  {nama: "Jeruk", harga: 30000, berat: 1000},
  {nama: "Mangga", harga: 30000, berat: 500}
]

class DataHargaBuah extends React.Component {
  render() {
    return (
      <div>
        <h1>Tabel Harga Buah</h1>
        <table>
          <thead>
            <tr>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
            </tr>
          </thead>
          <tbody>
            {dataHargaBuah.map(data => {
              return (
                <tr>
                  <FruitsName name={data.nama} />
                  <FruitsPrice price={data.harga} />
                  <FruitsWeight weight={data.berat} />
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    )
  }
}

export default DataHargaBuah