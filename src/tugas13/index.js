import React, {Component} from "react"
import './index.css'

class ListBuah extends Component{
    constructor(props){
        super(props)
        this.state = {
            dataHargaBuah: [
                {nama: "Semangka", harga: 10000, berat: 1000},
                {nama: "Anggur", harga: 40000, berat: 500},
                {nama: "Strawberry", harga: 30000, berat: 400},
                {nama: "Jeruk", harga: 30000, berat: 1000},
                {nama: "Mangga", harga: 30000, berat: 500}
            ],
            inputBuah: {
                nama: "",
                harga: "",
                berat: ""
            },
            indexOfForm: -1
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete =  this.handleDelete.bind(this);
    }

    handleChange(event){
        const {name, value} = event.target
        let inputBuah = {...this.state.inputBuah}
        inputBuah[name] = value

        this.setState({
            inputBuah
        })
    }

    handleEdit(event){
        let index = event.target.value
        let newBuah = this.state.dataHargaBuah[index]
        // console.log(newBuah)
        this.setState({
            inputBuah: {
                nama: newBuah.nama,
                harga: newBuah.harga,
                berat: newBuah.berat
            },
            indexOfForm: index
        })
    }

    handleDelete(event){
        // const { dataHargaBuah, indexOfForm } = this.state

        let index = event.target.value
        let newDataBuah = this.state.dataHargaBuah
        let editedBuah = newDataBuah[this.state.indexOfForm]
        newDataBuah.splice(index, 1)

        if (editedBuah !== undefined){
            var newIndex = newDataBuah.findIndex((el) => el === editedBuah)
            this.setState({
                dataHargaBuah: newDataBuah, 
                indexOfForm: newIndex
            })
        }else{
            this.setState({dataHargaBuah: newDataBuah})
        }
    }

    handleSubmit(event){
        event.preventDefault()
        // const { dataHargaBuah, inputBuah, indexOfForm} = this.state

        let newBuah = this.state.inputBuah
        // console.log(newBuah)
        if (newBuah["nama"].replace(/\s/g,'') !== "" && newBuah["harga"].toString().replace(/\s/g,'') !== "" && newBuah["berat"].toString().replace(/\s/g,'') !== ""){   
            let newDataBuah = this.state.dataHargaBuah
            let index = this.state.indexOfForm
            
            if (index === -1){
                newDataBuah = [...newDataBuah, newBuah]
            }else{
                newDataBuah[index] = newBuah
            }
            // console.log(newDataBuah)
            this.setState({
                dataHargaBuah: newDataBuah,
                inputBuah: {
                    nama: "",
                    harga: "",
                    berat: ""
                },
                indexOfForm: -1
            })
        }
    }

    render(){
        // console.log(this.state.dataHargaBuah)
        return(
        <>
            <h1>Tabel Harga Buah</h1>
            <table>
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        this.state.dataHargaBuah.map((data, index)=>{
                            return(                    
                                <tr key={index}>
                                    <td>{data.nama}</td>
                                    <td>{data.harga}</td>
                                    <td>{data.berat/1000} kg</td>
                                    <td>
                                        <button onClick={this.handleEdit} value={index}>Edit</button>
                                        &nbsp;
                                        <button onClick={this.handleDelete} value={index}>Delete</button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>

            <h1>Form Buah</h1>
            <form onSubmit={this.handleSubmit}>
                <label>Nama Buah:</label>          
                <input 
                    type="text"
                    name="nama"
                    value={this.state.inputBuah.nama}
                    onChange={this.handleChange}
                    placeholder="Nama Buah"
                /><br/>
                <label>Harga Buah:</label>          
                <input 
                    type="text" 
                    name="harga"
                    value={this.state.inputBuah.harga} 
                    onChange={this.handleChange}
                    placeholder="Harga Buah (Ribuan)"
                /><br/>
                <label>Berat Buah: </label>          
                <input 
                    type="text"
                    name="berat"
                    value={this.state.inputBuah.berat}
                    onChange={this.handleChange}
                    placeholder="Berat Buah (gr)"
                /><br/>
                <button>submit</button>
            </form>
        </>
        )
    }
}

export default ListBuah