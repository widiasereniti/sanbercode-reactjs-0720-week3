import React, {Component} from 'react'
import './index.css'

class Timer extends Component{
    constructor(props){
        super(props)
        this.state = {
            date: new Date(),
            time: 101
        }
    }

    componentDidMount(){
        this.timerID = setInterval(
            () => this.tick(),
            1000
        )
    }

    componentWillUnmount(){
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            date: new Date(),
            time: this.state.time - 1 
        });
    }

    render(){
        return (
            <>
                {this.state.time >= 0 && 
                    <div className="timer">
                        <h1 className="date">sekarang jam: {this.state.date.toLocaleTimeString()}</h1>
                        <h1 className="time">hitung mundur: {this.state.time}</h1>
                    </div>
                }
                <br/>
            </>
        )
    }
}

export default Timer