import React from 'react';
import './App.css';
import { BrowserRouter as Router } from "react-router-dom";
import Routes from './tugas15/Routes'
import Nav from './tugas15/NavColorList'

function App() {
  return (
    <div>
      <Router>
        {/* <Nav /> */}
        <Routes/>
      </Router> 
    </div>
  );
}

export default App;