import React, {useState, useEffect} from 'react';
import axios from "axios";
import './index.css';

const ListBuah = () => {
    const [dataHargaBuah, setHargaBuah] = useState(null)
    const [inputName, setInputName] = useState("")
    const [inputPrice, setInputPrice] = useState("")
    const [inputWeight, setInputWeight] = useState("")
    const [selectedId, setSelectedId]  =  useState(0)
    const [statusForm, setStatusForm]  =  useState("create")

    useEffect( () => {
        if (dataHargaBuah === null){
        axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
            .then(res => {
                setHargaBuah(res.data.map(el => { 
                    return {
                        id: el.id, 
                        name: el.name, 
                        price: el.price, 
                        weight: el.weight
                    }
                }))
            })
        }
    }, [dataHargaBuah])

    const handleDelete = (event) => {
        const { value } = event.target
        let idBuah = parseInt(value)

        let newDataHargaBuah = dataHargaBuah.filter(item => item.id !== idBuah)
        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
        .then(res=>{
            console.log(res);
        })

        setHargaBuah([...newDataHargaBuah])
    }

    const handleEdit = (event) =>{
        const { value } = event.target
        let idBuah = parseInt(value)
        let newBuah = dataHargaBuah.find(x=> x.id === idBuah)
        
        setInputName(newBuah.name)
        setInputPrice(newBuah.price)
        setInputWeight(newBuah.weight)
        setSelectedId(idBuah)
        setStatusForm("edit")
    }

    const handleChange = (event) =>{
            const { name, value } = event.target
            
            switch(name){
                case "inputName":
                    setInputName(value);
                    break;
                case "inputPrice":
                    setInputPrice(value);
                    break;
                case "inputWeight":
                    setInputWeight(value);
                    break;
                default:
                    break;
            }
    }

    const handleSubmit = (event) =>{

        event.preventDefault()
        let name = inputName
        let price = inputPrice
        let weight = inputWeight
        // let newBuah = {name, price, weight}

        if (name.replace(/\s/g,'') !== "" && price.toString().replace(/\s/g,'') !== "" && weight.toString().replace(/\s/g,'') !== ""){
            if (statusForm === "create"){
                axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name, price, weight})
                .then(res => {
                    setHargaBuah([...dataHargaBuah, {
                        id: res.data.id,
                        name: name,
                        price: price,
                        weight: weight
                    }])
                })
            }else if(statusForm === "edit"){
                axios.put(`http://backendexample.sanbercloud.com/api/fruits/${selectedId}`, {name, price, weight})
                .then(res => {
                    let dataBuah = dataHargaBuah.find(el=> el.id === selectedId)
                    dataBuah.name = name
                    dataBuah.price = price
                    dataBuah.weight = weight
                    setHargaBuah([...dataHargaBuah])
                })
            }

            setStatusForm("create")
            setSelectedId(0)
            setInputName("")
            setInputPrice("")
            setInputWeight("")
        }
    }

    return (
        <>
            <h1>Tabel Harga Buah</h1>
            <table>
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        dataHargaBuah !== null && dataHargaBuah.map((data, index)=>{
                            return(                    
                                <tr key={index}>
                                    <td>{data.name}</td>
                                    <td>{data.price}</td>
                                    <td>{data.weight/1000} kg</td>
                                    <td>
                                        <button onClick={handleEdit} value={data.id}>Edit</button>
                                        &nbsp;
                                        <button onClick={handleDelete} value={data.id}>Delete</button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>

            <h1>Form Buah</h1>
            <form onSubmit={handleSubmit}>
                <label>Nama Buah:</label>          
                <input 
                    type="text"
                    name="inputName"
                    // value={inputBuah.name}
                    value={inputName}
                    onChange={handleChange}
                    placeholder="Nama Buah"
                /><br/>
                <label>Harga Buah:</label>          
                <input 
                    type="text" 
                    name="inputPrice"
                    // value={inputBuah.price} 
                    value={inputPrice}
                    onChange={handleChange}
                    placeholder="Harga Buah (Ribuan)"
                /><br/>
                <label>Berat Buah: </label>          
                <input 
                    type="text"
                    name="inputWeight"
                    // value={inputBuah.weight}
                    value={inputWeight}
                    onChange={handleChange}
                    placeholder="Berat Buah (gr)"
                /><br/>
                <button>submit</button>
            </form>
        </>
    )
}

export default ListBuah